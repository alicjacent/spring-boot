package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Lalu on 17.04.2018.
 */

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "hello world!";
    }
}
